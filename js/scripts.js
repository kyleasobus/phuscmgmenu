menuInit();

function menuInit() {
    $(window).width() < 768 ? mobile = true : mobile = false;

    var menuToggle = document.getElementById("menuToggle");
    //var menuClose = document.getElementById("menuClose");
    var fadeToggle = document.getElementById("fade");
    menuToggle.addEventListener("click", function () {
        toggleMenu('main-menu', 'fade')
    });
    fadeToggle.addEventListener("click", function () {
        e.preventDefault();
        toggleMenu($(this).data('menu'), 'fade');
    });

    var subNavs = $('#main-menu li').has("ul").children("a");
    var currentlyAnimating = false;

	if(mobile) {
        $(subNavs).on("click", function (e) {
            if (currentlyAnimating) {
                return;
            }
            currentlyAnimating = true;
            getSubMenu(this);
            currentlyAnimating = false;
        });
    }
}

function getSubMenu(elem) {
    p = $(elem).parent();
        if($(p).hasClass('active')) { //Exit Submenu
            $(p).parent().removeClass('active');
            $(p).children('ul').fadeOut(250, function () {
                $(p).siblings().slideDown(250);
                $(p).removeClass('active prev').find('.active').removeClass('active');
                $(p).children('ul').find('li.active>ul').hide()
            });
            $(p).siblings('ul').find('.active').removeClass().hide();
        } else { //Enter Submenu
            $('.listNav .current').removeClass('current');
            $(p).parents('li.subNav').addClass('prev');
            $(p).addClass('active').parent().addClass('active');
            $(p).siblings().animate({
                opacity: 0,
                marginLeft: "-100%"
              }, {
                duration: "100",
                ease: "ease"
            }).slideUp(0).fadeOut(200, function () {
                $(p).siblings().css('opacity','1').css('margin-left','0');
                $(p).children('ul').addClass('current').fadeIn();
            });
            $(p).parents('ul.subnav.active').addClass('disable')
        }
        return false;
    }

function toggleMenu(menuId, fadeId) {
    var menu = document.getElementById(menuId);
    var fade = document.getElementById(fadeId);
    $(menu, fade).toggleClass('active');
    $(fade).attr('data-menu', menuId);
    $(fade).fadeToggle();
}
toggleMenu("doctorMenu", "fade");
toggleMenu("testMenu", "fade");
(function ($) {

    $(document).ready(function () {



    });

})(jQuery);